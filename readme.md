# Party Room

## REST api

This api is documented using [openapi3][3], and how cool is it that gitlab generated the docs!! Just scoot over [here][1] to take a look.

## Database schema

Basic schema docs are written in [DBML][4] and hosted on [dbdiagram.io][2]

[1]: https://gitlab.com/party-room/api/-/blob/master/docs/openapi/openapi.yml
[2]: https://dbdiagram.io/d/5fe2a1cb9a6c525a03bc0ce5
[3]: https://swagger.io/specification/
[4]: https://www.dbml.org/home/#intro
