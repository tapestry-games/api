ALTER TABLE `skill` 
  ADD CONSTRAINT fk_skill_player 
  FOREIGN KEY (`player`) 
  REFERENCES `player` (`id`);
