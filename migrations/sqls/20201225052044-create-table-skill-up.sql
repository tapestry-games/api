CREATE TABLE `skill` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255),
  `logic` text NOT NULL COMMENT 'this is a base64 encoded js script',
  `player` int NOT NULL
);