/* Replace with your SQL commands */
CREATE TABLE `room` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `uuid` varchar(255) UNIQUE NOT NULL COMMENT 'generated uuid',
  `name` varchar(255) NOT NULL,
  `context` mediumtext COMMENT 'this is a base64 encoded js script'
);