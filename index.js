'use strict'

const DBMigrate = require('db-migrate')
const express = require('express')
const dotenv = require('dotenv')
const banner = require('./assets/banner')
const uuid = require('uuid').v4
const bunyan = require('bunyan')

// configuration
const appConfig = dotenv.config({ path: 'config.app.env' })
const dbConfig = dotenv.config({ path: 'config.db.env' })
const host = process.env.HOST
const port = process.env.PORT
const app = express()
const log = bunyan.createLogger({
  name: 'app',
  level: process.env.LOG_LEVEL,
})

// routes
const router = express.Router()
const acceptsJson = express.json()

router.get('/rooms', (req, res) => {
  res.json([
    {
      uuid: '8fb6cc78-9ac4-461c-80f5-345bdaf52701',
      name: 'First City',
    },
  ])
})

router.post('/rooms', acceptsJson, (req, res) => {
  const roomName = req.body.name
  log.debug(req.body)

  if (roomName) {
    res.json({
      uuid: uuid(),
      name: roomName,
    })
  } else {
    res.status(400).json({ error: 'Bad Request' })
  }
})

app.use('/', router)

// apply all migrations
DBMigrate.getInstance(true).up()

app.listen(port, () => {
  console.log(banner)
  console.log(`listening at: ${host}:${port}\n\n`)
})
